/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package calculator;


import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.FlowLayout;

import java.awt.GridBagLayout;



import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author
 */
public class MainFrame extends javax.swing.JFrame {
    private  JTextField textField;
    String operation = "";
    boolean isOperation = false;
    int firstValue = 0;
    int secondValue = 0;
    String finalResult;
    
     
    public MainFrame(String title)
    {
        super(title);
        
        Container contentPane = this.getContentPane();
        
        JPanel panel = new JPanel();
        contentPane.add(panel);
        
        textField = new JTextField();
        panel.add(textField, BorderLayout.CENTER);
        textField.setPreferredSize(new Dimension(350, 40));
        textField.setText("0");
        
        
        for(int i = 0; i<10; i++)
        {
            JButton button = new JButton(Integer.toString(i));
            panel.add(button);            
            button.setPreferredSize(new Dimension(50,40));
            
            button.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) 
            {
            JButton btn =(JButton)e.getSource();
            setTextFieldValue(btn);                              
            }
        });
        }  
          JButton buttonSum = new JButton();
          panel.add(buttonSum);
          buttonSum.setText("+");
          buttonSum.setPreferredSize(new Dimension(50,40));
          
          JButton buttonMinus = new JButton();
          panel.add(buttonMinus);
          buttonMinus.setText("-");
          buttonMinus.setPreferredSize(new Dimension(50,40));
          
          JButton buttonMultiply = new JButton();
          panel.add(buttonMultiply);
          buttonMultiply.setText("*");
          buttonMultiply.setPreferredSize(new Dimension(50,40));
          
          JButton buttonDivide = new JButton();
          panel.add(buttonDivide);
          buttonDivide.setText("/");   
          buttonDivide.setPreferredSize(new Dimension(50,40));
          
          JButton buttonEqually = new JButton();
          panel.add(buttonEqually);
          buttonEqually.setText("=");
          buttonEqually.setPreferredSize(new Dimension(50,40));
          
          JButton buttonClear = new JButton();
          panel.add(buttonClear);
          buttonClear.setText("C");
          buttonClear.setPreferredSize(new Dimension(50,40));
          
          buttonSum.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
          JButton btnActions =(JButton) e.getSource();
          //setTextFieldValue(btnActions); v.2
         
          operation = "+";
          isOperation = true;
          firstValue = Integer.parseInt(textField.getText());
          
          
          }
        });
          
          buttonMinus.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
          JButton btnActions =(JButton) e.getSource();
          //setTextFieldValue(btnActions);v.2
          
          operation = "-";
          isOperation = true;
          firstValue = Integer.parseInt(textField.getText());
          }
        });
          
          buttonMultiply.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
          JButton btnActions =(JButton) e.getSource();
          //setTextFieldValue(btnActions);   v.2
          
          operation = "*";
          isOperation = true;
          firstValue = Integer.parseInt(textField.getText());
          }
        });
        
          buttonDivide.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
          JButton btnActions =(JButton) e.getSource();
          //setTextFieldValue(btnActions); v.2
          
          operation = "/";
          isOperation = true;
          firstValue = Integer.parseInt(textField.getText());
          }          
        });
          buttonEqually.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
          secondValue = Integer.parseInt(textField.getText());
          
            //if("+" == operation)
              if(operation.equals("+"))
              finalResult = Integer.toString(firstValue + secondValue);
              textField.setText(finalResult);
              
            //if("-" == operation)
              if(operation.equals("-"))
              finalResult = Double.toString(firstValue - secondValue);
              textField.setText(finalResult);
              
            //if("/" == operation)
              if(operation.equals("/")) {           
                  try {
                      finalResult = Double.toString(firstValue / secondValue);
                      if(finalResult.equals(0))
              {
                  System.out.println("Exception");
              }
              else
              {
                  textField.setText(finalResult);
              }
                      } 
                  catch (Exception ex) 
                  {
                      textField.setText("Error, divide zero");
                      return;
                  }
              
              
              
              }
              
            //if("*" == operation)
              if(operation.equals("*"))
              finalResult = Double.toString(firstValue * secondValue);
              textField.setText(finalResult);
              
              
              
          }
        });
          
          buttonClear.addActionListener(new ActionListener() {
          @Override
          public void actionPerformed(ActionEvent e) {
              textField.setText("");              
          }
        });
          
    
                            
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
    }
    
    private void setTextFieldValue(JButton btn){
         String s = textField.getText();
         if(s.isEmpty())           
         {
         s = btn.getText();  
         if(s.equals("+") || s.equals("-") || s.equals("*") || s.equals("/") || s.equals("0"))
         {
            s = "";
         }
         }
         else
         {
         if(isOperation)
         {
            s = btn.getText();  
            isOperation = false;                       
         }
         else
         {
         if(s.equals("0"))
         {
            s = "";
         }
            s += btn.getText();  
         }                     
         }
            textField.setText(s); 
                
    }
}
